const moment = require('moment');
const bodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const Koa = require('koa');
const cors = require('koa2-cors');

let BlogManager = require('./Blog/BlogManager');
let ApplyMysql = require('./Blog/applyMysql');

async function main () {
    let Blog = new BlogManager();
    let AppMysql = new ApplyMysql();
    global.instance = {
        DBHandler:AppMysql,
        BlogHandler:Blog
    }
    const app = new Koa();
    app.use(bodyParser());
    app.use(cors());
    app.use(async (ctx,next) =>{
        console.log(ctx.originalUrl);
        await next();
    })

    let router = new Router();
    const baseRouter = require('./router/index');
    router.use('/',baseRouter);
    app.use(router.routes()); //启动路由
    app.use(router.allowedMethods());

    app.listen(3000,()=>{
        console.log("服务启动，监听于3000端口 http://127.0.0.1:3000");
    })
    
}


main();
