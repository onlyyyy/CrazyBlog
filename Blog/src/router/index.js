import Vue from 'vue'
import Router from 'vue-router'
import blogs from '@/view/blogs'
import manager from '@/view/manager'
import Main from '@/layout'
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'

Vue.use(Router)
Vue.use(ViewUI)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'menu',
      component: Main,
      children: [
        {
          path: '/home/blogs',
          name: '/home/blogs',
          component: blogs
        },
        {
          path: '/home/manager',
          name: '/home/manager',
          component: manager
        }
      ]
    }
  ]
})
