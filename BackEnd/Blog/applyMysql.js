
var mysql = require('mysql');

const connectionInfo = {
    host:'localhost',
    user:'root',
    password:'Djy319677'
}

class ApplyMysql {
    constructor() {
        
    }


    async dbOp(sql, sqlParam) {
        var connection = null;

        connection = mysql.createConnection(connectionInfo);  
        
        
        return await this.dbOp1(sql, sqlParam, connection);
    }

    async beginTrans() {
        var connection = null;
        if (this.bUsePool) {
            connection = await this.getConnectionFromPool();
        } else {
            connection = mysql.createConnection(this.connectionInfo);  
        }

        return await this.beginTrans1(connection);
    }

    dbOp1(sql, sqlParam, connection) { 
        var promise = new Promise((resolve, reject) => {
            connection.query(sql, sqlParam, (err, result) => {
                if (err) {
                    console.log('[dbOp1 error] - ' + err.message);
                    //connection.destroy();
                    reject(err);
                } else {
                    //connection.destroy();
                    resolve(result);
                }

                
                connection.destroy();
                
            });
        });

        return promise;
    }

   
    beginTrans1(connection) {
        var promise = new Promise((resolve, reject) => {
            connection.beginTransaction((err) => {
                if (err) {
                    console.log('[beginTrans1 error] - ' + err.message);
                    reject(err);
                } else {
                    resolve(connection);
                }
            });
        });

        return promise;
    }

    commit(connection) {
        var promise = new Promise((resolve, reject) => {
            connection.commit((err) => {
                if (err) {
                    console.log('[beginTrans error] - ' + err.message);
                    reject(err);
                } else {
                    resolve(connection);
                }


                connection.destroy();
                
            });
        });

        return promise;
    }

    rollback(connection) {
        var promise = new Promise((resolve, reject) => {
            connection.rollback((err) => {
                if (err) {
                    console.log('[beginTrans error] - ' + err.message);
                    reject(err);
                } else {
                    resolve(connection);
                }

                connection.destroy();
                
            });
        });

        return promise;
    }

    dbOpInTrans(sql, sqlParam, connection) {
        var promise = new Promise((resolve, reject) => {
            connection.query(sql, sqlParam, (err, result) => {
                if (err) {
                    console.log('[dbOpInTrans error] - ' + err.message);
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });

        return promise;
    }


  
    async getPostListDB() {
        let sql = `select Name,Blogs,Type,createTime from crazyblog.t_blogs`;
        console.log("获取文章列表的SQL为",sql);
        try{
            let result = await this.dbOp(sql);
            return result;
        }catch (error) {
            console.log("getPostListDB dbop error->",error);
        }
    }



}

module.exports = ApplyMysql;